const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const start = () => {
  console.log("Buka Web Academy Glints");
  glintTest();
};

const glintTest = () => {
  console.log("process test Bahasa inggris, logika dan research");
  ifPassed();
};

const interviewTest = () => {
  console.log("Tes Wawancara");
  inputDocument();
};

const inputDocument = () => {
  console.log("Input Dokumen dan Pembayaran");
  chooseStack();
};

function chooseStack() {
  console.log("PIlih Stack");

  console.log(`1. BE`);
  console.log(`2. AE`);
  console.log(`3. RN`);
}

function passed(stack) {
  console.log(`Selamat Anda Lulus di ${stack}`);
}

function ifPassed(result) {
  rl.question("Apakah anda lulus tes awal? y/t ", (result) => {
    if (result == "y") {
      interviewTest();
    } else if (result == "t") {
      console.log("Anda Belum Lulus");
    } else {
      console.log("masukan salah");
      start();
    }

    rl.question(`Choose option: `, (option) => {
      if (option == 1) {
        passed("Backend");
      } else if (option == 2) {
        passed("Frontend");
      } else if (option == 3) {
        passed("React Native");
      } else {
        console.log(`Option must be 1 to 3!\n`);
      }

      rl.close();
    });
  });
}
start();
