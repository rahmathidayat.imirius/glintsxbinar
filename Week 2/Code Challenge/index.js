const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */
let x = data.length;

for (i = 0; i < x; i++) data[i] && data.push(data[i]); // copy non-empty values of data to the end of the array

data.splice(0, x);
console.log("Data Random tanpa null:");
console.log(data);
// Optional
// function clean(data) {
//   return data.filter((i) => typeof i === "number");
// }

// Should return array
function sortAscending(data) {
  // Code Here
  const data1 = data.slice();
  for (var i = 0; i < data1.length; i++) {
    for (var j = 0; j < data1.length - i - 1; j++) {
      if (data1[j] > data1[j + 1]) {
        // If the condition is true then swap them
        var temp = data1[j];
        data1[j] = data1[j + 1];
        data1[j + 1] = temp;
      }
    }
  }

  return data1;
}

// Should return array
function sortDecending(data) {
  // Code Here
  const data2 = data.slice();
  for (var i = 0; i < data2.length; i++) {
    for (var j = 0; j < data.length - i - 1; j++) {
      if (data2[j] < data2[j + 1]) {
        // If the condition is true then swap them
        var temp = data2[j];
        data2[j] = data2[j + 1];
        data2[j + 1] = temp;
      }
    }
  }

  return data2;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
