const index = require('../index.js');

const prismVolume = (basearea, height) => {
  return (basearea * height);
}

const getBaseArea = () => {
  console.log("You want to calculate prism volume ? ")
  index.rl.question("Give me base area ? ", baseArea => {
    if(!isNaN(baseArea) && !index.isEmptyOrSpaces(baseArea)){
      getHeight(baseArea)
    } else {
      console.log("I'm sorry wrong input")
      getBaseArea()
    }
  })
}

const getHeight = (baseArea) => {
  index.rl.question('Give me height ? ', height => {
    if(!isNaN(height) && !index.isEmptyOrSpaces(height)){
      console.log(`\nYour Prism Volume is : ${prismVolume(baseArea, height)}`)
      index.rl.close()
    } else {
      console.log("I'm sorry wrong input")
      getHeight(baseArea)
    }
  })
}

module.exports = getBaseArea;