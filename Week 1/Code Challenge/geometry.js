//rumus volme bola
// V = 4/3 × π × r³
console.log("JScript Hitung Hasil Tambah 2 Volume Bola");
console.log("");

 
function hitungVolumeBola(r) {
  const luasLingkaran = Math.PI * r ** 2;  
  const volumeBola = (luasLingkaran * 4) / 3;

  console.log(`Volume Bola =  ${volumeBola} cm3`);
  
  return volumeBola;
}

let Bola1 = hitungVolumeBola(4);
let Bola2 = hitungVolumeBola(10);

let hasiltambahBolaAB = Bola1 + Bola2;
console.log("");
console.log(`Volume Bola A + B = ${hasiltambahBolaAB} cm3`);
console.log("");

//rumus volme Limas Segi Empat dengan Alas Persegi
// V = ⅓ × L alas × t
// La = s × s

console.log("JScript Hitung Hasil Kali 2 Limas Segi Empat dengan Alas Persegi");
console.log("");
function hitungVolumeLimas(s,t) {
  const luasAlas = s ** 2;  
  const volumeLimas = (luasAlas * t) / 3;

  console.log(`Volume Limas =  ${volumeLimas} cm3`);
  
  return volumeLimas;
}

let Limas1 = hitungVolumeLimas(4,9);
let Limas2 = hitungVolumeLimas(5,6);

let hasilKaliLimasAB = Limas1 * Limas2;
console.log("");
console.log(`Volume Limas A x B = ${hasilKaliLimasAB} cm3`);