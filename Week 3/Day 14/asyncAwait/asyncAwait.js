//import fs
const fs = require("fs");

//make promise object
const readFile = (file, option) =>
  new Promise((fullfilled, rejected) => {
    fs.readFile(file, option, (err, content) => {
      if (err) rejected(err);
      return fullfilled(content);
    });
  });

const readAllFiles = async () => {
  try {
    let players = await Promise.all([
      readFile("./contents/content1.txt", "utf-8"),
      readFile("./contents/content2.txt", "utf-8"),
      readFile("./contents/content3.txt", "utf-8"),
      readFile("./contents/content4.txt", "utf-8"),
      readFile("./contents/content5.txt", "utf-8"),
      readFile("./contents/content6.txt", "utf-8"),
      readFile("./contents/content7.txt", "utf-8"),
      readFile("./contents/content8.txt", "utf-8"),
      readFile("./contents/content9.txt", "utf-8"),
      readFile("./contents/content10.txt", "utf-8"),
    ]);

    for (let p = 0; p < players.length; p++) {
      console.log(players[p]);
    }
  } catch (e) {
    console.error(e);
  }
};

readAllFiles();
