const { Square, Rectangle, Vcube, Vcone, Vbeam, Vtube} = require("./geometry");

// let squareOne = new Square(12);
// squareOne.introduce("Hei you,");
// squareOne.calculateArea();

// let rectangleOne = new Rectangle(11, 12);
// rectangleOne.calculateArea();
// rectangleOne.calculateCircumference();

let vCube1 = new Vcube(10);
vCube1.calculateVolume();

let vCone2 = new Vcone(5,5);
vCone2.calculateVolume();

let vBeam3 = new Vbeam(3,4,5);
vBeam3.calculateVolume();

let vTube4 = new Vtube(6,7);
vTube4.calculateVolume();
