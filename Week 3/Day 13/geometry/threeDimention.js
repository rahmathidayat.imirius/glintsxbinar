const Geometry = require("./geometry");

class ThreeDimention extends Geometry {
  constructor(name) {
    super(name, "3D");

    if (this.constructor == ThreeDimention) {
      throw new Eror(`Declare object not Allowed!`);
    }
  }
  introduce() {
    super.introduce();
    console.log(`This is ${this.type} Section`);
  }

  calculateVolume() {
    console.log(`Calculate Volume of ${this.name}`);
  }
}

module.exports = ThreeDimention;
