const Square = require("./2D/square");
const Rectangle = require("./2D/rectangle");
const Triangle = require("./2D/triangle");
const Vcube = require("./3D/vCube");
const Vtube = require("./3D/vTube");
const Vcone = require("./3D/vCone");
const Vbeam = require("./3D/vBeam");



module.exports = { Square, Rectangle, Triangle, Vcube, Vbeam, Vcone, Vtube};
