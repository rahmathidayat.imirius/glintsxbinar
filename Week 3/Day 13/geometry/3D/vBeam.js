const ThreeDimention = require("../threeDimention");

class Vbeam extends ThreeDimention {
  constructor(length,width,height) {
    super("Beam");

    this.length = length;
    this.width = width;
    this.height = height;
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}`);
  }

  calculateVolume() {
    super.calculateVolume();
    let volBeam = this.length * this.width * this.height;
    console.log(`${this.name} Volume is: ${volBeam} cm3\n`);
  }
}

module.exports = Vbeam;
