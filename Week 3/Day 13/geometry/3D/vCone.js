const ThreeDimention = require("../threeDimention");

class Vcone extends ThreeDimention {
  constructor(radius,height) {
    super("Cone");

    this.radius = radius;
    this.height = height;
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}`);
  }

  calculateVolume() {
    super.calculateVolume();
    let volCone = (1 / 3) * Math.PI * this.radius ** 2 * this.height;
    console.log(`${this.name} Volume is: ${volCone} cm3\n`);
  }
}

module.exports = Vcone;
