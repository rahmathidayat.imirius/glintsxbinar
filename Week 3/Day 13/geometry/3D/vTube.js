const ThreeDimention = require("../threeDimention");

class Vtube extends ThreeDimention {
  constructor(radius,height) {
    super("Tube");

    this.radius = radius;
    this.height = height;
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}`);
  }

  calculateVolume() {
    super.calculateVolume();
    let volTube = Math.PI * (this.radius**2) * this.height;
    console.log(`${this.name} Volume is: ${volTube} cm3\n`);
  }
}

module.exports = Vtube;
