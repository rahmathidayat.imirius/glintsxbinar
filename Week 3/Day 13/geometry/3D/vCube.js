const ThreeDimention = require("../threeDimention");

class Vcube extends ThreeDimention {
  constructor(length) {
    super("Cube");

    this.length = length;
  }

  introduce() {
    super.introduce();
    console.log(`This is ${this.name}`);
  }

  calculateVolume() {
    super.calculateVolume();
    let vol = this.length ** 3;
    console.log(`${this.name} Volume is: ${vol} cm3\n`);
  }
}

module.exports = Vcube;
