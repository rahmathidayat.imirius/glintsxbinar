class HelloController {
  get(req, res) {
    console.log("This is my GET first backend try");
    //res.send(`Hello, Postman (GET) ${req.query.name}`);
    res.send(`${req.params.name} Hidayat`);
  }
  post(req, res) {
    console.log("This is my POST first backend try");
    res.send("Hello, Postman (POST)");
  }
  put(req, res) {
    console.log("This is my PUT first backend try");
    res.send("Hello, Postman (PUT)");
  }
  delete(req, res) {
    console.log("This is my  DELETE first backend try");
    res.send("Hello, Postman (DELETE)");
  }
}

module.exports = new HelloController();
